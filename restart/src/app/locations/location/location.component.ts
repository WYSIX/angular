import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  constructor() { }

  locations = [];

  newLocation: string = '';

  ngOnInit() {
    this.locations = ["location 1", "location 2"];
  }

  addLocation() {
    this.locations.push(this.newLocation);
  }

  textValue = 'initial value';
  log = '';

  logText(value: string): void {
    this.log += `Text changed to '${value}'\n`;
  }

}
