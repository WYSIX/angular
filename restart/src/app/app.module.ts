import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TripService, ApiService} from './shared';
import { HeaderComponent } from './header/header.component';
import { TriplistComponent } from './triplist/triplist.component';

import { LocationsModule } from './locations/locations.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TriplistComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    LocationsModule
  ],
  providers: [
  	ApiService,
  	TripService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
