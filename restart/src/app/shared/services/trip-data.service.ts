import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class TripDataService {

  trips = [{
  	title: 'Trip to London'
  }, {
  	title: 'Trip to Australia'
  } ,{
  	title: 'Trip to Barcelona'
  }];

  constructor(private api: ApiService) { }

  getAllTrips() {
    return this.trips;
  	
  }

  addTrip(trip) {
  	this.trips.push(trip);
  }

  getTrip(id) {
  	return this.trips[id];
  }

  deleteTrip(trip) {
  	let index = this.trips.indexOf(trip);
  	if (index > -1) {
  		this.trips.splice(index,1);
  	}
  }

  updateTrip(trip) {
  	let index = this.trips.indexOf(trip);
  	if (index > -1) {
  		this.trips[index] = trip;
  	}
  }
}
