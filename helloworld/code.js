(function () {
    "use strict";

    window.addEventListener('load', function () {
        const clickMeButton = document.querySelector('.button');
        const myText = document.querySelector('#myText');

        clickMeButton.addEventListener('click', function () {
            let currentClass = myText.className;
            if (currentClass === "present") {
                myText.className = "hide";
            } else {
                myText.className = "present";
            }
        });
    })
})();
