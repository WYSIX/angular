import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-triplist',
  templateUrl: './triplist.component.html',
  styleUrls: ['./triplist.component.css']
})
export class TriplistComponent implements OnInit {

  @Input() adding_trip;
  @Input() trips;

  @Output() onDeleteTrip = new EventEmitter();
  @Output() onEditTrip = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  deleteTrip(trip) {
    this.onDeleteTrip.emit(trip);
  }

  editTrip(trip) {
    this.onEditTrip.emit(trip);
  }

}
